﻿using Microsoft.Win32;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace proyectoParalelismoArqui
{
    
    static class Program
    {
        public static Thread hiloMain;

        public static string[] arrayPaginasWeb = {

            "http://www.teletica.com",
            "http://www.nacion.com",
            "http://www.repretel.com",
            "http://www.ovsicori.una.ac.cr",
            "http://www.microsoft.com",
            "http://www.amazon.com",
            "http://www.msn.com",
            "http://www.bbc.com/mundo",
            "http://cnnespanol.cnn.com",
            "http://www.minutouno.com",
            "http://www.talcualdigital.com",
            "http://news.google.com.mx",
            "http://www.univision.com/noticias",
            "http://www.laprensalibre.cr",
            "http://www.diarioextra.com",
            "http://www.elpais.com",
            "http://espndeportes.espn.com",
            "http://deportes.elpais.com",
            "http://www.abc.es",
            "http://www.actualidadgadget.com",
            "http://www.bloginformatico.com",
            "http://uce-ing-informatica.blogspot.com",
            "http://www.elladodelmal.com",
            "http://profesoradeinformatica.com",
            "http://xataca.com"
        };
        
        static void Main()
        {

            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            Console.WriteLine("iniciado*");
            Application.Run(new main());


        }



    }
}
