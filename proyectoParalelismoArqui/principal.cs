﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading;
using System.Windows.Forms;
using System.Diagnostics;
using System.Text.RegularExpressions;
using System.Net;
using System.Threading.Tasks;

namespace proyectoParalelismoArqui
{


    public partial class Form1 : Form
    {
        //***
        public static int cores = 0;
        static Stopwatch temporizador;
        private Thread hiloCPU;
        public Thread hiloVentanaResultados;
        private double[] arregloCPU = new double[30];
        List<String> listaPaginasResultado = new List<string>();
        List<String> listaIncidenciasResultado = new List<string>();
        List<String> listaTiempoResultado = new List<string>();
        List<String> listaPalabrasResultado = new List<string>();
        string labelTiempo = "";
        bool bandera = false;

        public void setCores(int a) {
            cores = a;
            Console.WriteLine("set: "+cores);

        }
        public static string DescargarWeb(string url)
        {

            var client = new WebClient { Encoding = System.Text.Encoding.UTF8 };            // { Encoding = System.Text.Encoding.UTF8 } es para que lea tildes y letra ñ
            var text = client.DownloadString(url);                                          //Desgarga la pagina web

            string webSinEtiquetasHTML = Regex.Replace(text, @"\r\n", "");        //para quitar los tags de html

            //Regex rRemScript = new Regex(@"\n");
            //output = rRemScript.Replace(input, "");
            //Console.WriteLine(webSinEtiquetasHTML);
            //Console.WriteLine("**************************************************************************************************************");
            Console.WriteLine();
            //Console.WriteLine(webSinEtiquetasHTML);
            Console.WriteLine();

            return webSinEtiquetasHTML;
        }//fin DescargarWeb

        private void rendimientoCPU() {
            var contadorRendimientoCPU = new PerformanceCounter("Processor Information", "% Processor Time", "_Total");


            while (true) {

                arregloCPU[arregloCPU.Length - 1] = Math.Round(contadorRendimientoCPU.NextValue(), 0);
                Array.Copy(arregloCPU, 1, arregloCPU, 0, arregloCPU.Length-1);
                if (graficoCPU.IsHandleCreated)
                {
                    this.Invoke((MethodInvoker)delegate { actualizarGraficoCPU(); });

                }
                else {

                    //...
                }
                Thread.Sleep(1000);
              

            }//fin while

        }//fin rendimientoCPU
        //***


        private void actualizarGraficoCPU() {
            graficoCPU.Series[" USO_CPU"].Points.Clear();

            for (int i = 0; i < arregloCPU.Length; i++) {
                graficoCPU.Series[" USO_CPU"].Points.AddY(arregloCPU[i]);

            }
        }


        public static List<String> palabras;

        public Form1()
        {
            InitializeComponent();

        }

        private void label1_Click(object sender, EventArgs e)
        {

        }

        private void button1_Click(object sender, EventArgs e)
        {
            palabras = new List<string>();
            List<String> listaPaginasResultado = new List<string>();
            List<String> listaIncidenciasResultado = new List<string>();
            List<String> listaTiempoResultado = new List<string>();
            List<String> listaPalabrasResultado = new List<string>();
            string labelTiempo = "";
            
            string busqueda =textBoxBuscar.Text.ToString();
            busqueda += ';';
            
            string aux = "";
         
            for (int i=0; i<busqueda.Length; i++) {     //se toman las palabras o frases a buscar y se guardan en un list
                if (busqueda[i] != ';')
                {
                    aux += busqueda[i];
                }
                else {
                    palabras.Add(aux);
                    Console.WriteLine("palabra "+i+": "+aux);
                    aux = "";
                }

                
            }

            this.graficoCPU.Visible=true;
            this.label2.Visible = true;
            this.label3.Visible = true;
            


            hiloCPU = new Thread(new ThreadStart(this.rendimientoCPU));
            hiloCPU.IsBackground = true;
            hiloCPU.Start();
            
            main opcionBusqueda = new main();
            opcionBusqueda.Show();
            cores = opcionBusqueda.cores;
            Console.WriteLine("cores cadena: "+cores);




        }

        public void llamar(char modalidad) {
            if (modalidad == 's')
            {
                hiloVentanaResultados = new Thread(new ThreadStart(buscarSecuencial));
                hiloVentanaResultados.IsBackground = true;
                bandera = true;
                hiloVentanaResultados.Start();
            }
            else {
                hiloVentanaResultados = new Thread(new ThreadStart(busquedaParalela));
                hiloVentanaResultados.IsBackground = true;
                bandera = true;
                hiloVentanaResultados.Start();

            }
            
            
            
            return;
        }

        
        public void mostrarResultados() {

            ventanaResultados v2 = new ventanaResultados();
            Console.WriteLine("cantidad de paginas: " + listaPaginasResultado.Count());
            int index = 0;
            foreach (string link in listaPaginasResultado)
            {
                ListViewItem list = new ListViewItem(link);
                list.Remove();
                list.SubItems.Add(Convert.ToString(listaIncidenciasResultado.ElementAt(index)));
                list.SubItems.Add(Convert.ToString(listaTiempoResultado.ElementAt(index)));
                list.SubItems.Add(Convert.ToString(listaPalabrasResultado.ElementAt(index)));
                v2.label2.Text = "Tiempo ejecución: "+temporizador.Elapsed;

                v2.listView1.Items.Add(list);
                index++;
            }
            v2.Show();

        }
        private void Form1_Load(object sender, EventArgs e)
        {

        }

        public void buscarSecuencial()
        {


            string found = "";
            int contadorIndices = 0;

            temporizador = Stopwatch.StartNew();


            foreach (string busqueda in palabras)
            {
                Console.WriteLine("palabra a buscar es: "+busqueda);
                for (int x = 0; x < Program.arrayPaginasWeb.Length; x++)
                {

                    int contadorIncidencias = 0;
                    bool banderaPagina = false;
                    Stopwatch tiempoPagina = Stopwatch.StartNew();


                    string pag = DescargarWeb(Program.arrayPaginasWeb[x]);

                    for (int i = 0; i < pag.Length; i++)
                    {

                        int cont = i; //index auxiliar para concatenacion
                        int contadorWhile = 0; //contador de parada para el while
                        found = "";
                        while (contadorWhile < busqueda.Length)//busqueda es la palabra o frase a buscar
                        {
                            if (cont >= pag.Length)
                            {
                                break;      //para evitar salirse del rango si se llega al final
                            }
                            found += pag[cont]; //concatenacion de caracteres
                            cont++;             //se suma el indice auxiliar
                            contadorWhile++;    //se suma el contador de condicion de parada

                        }
                        if (found == busqueda)
                        {
                            Console.WriteLine("<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<-->  " + found + " en pagina: " + Program.arrayPaginasWeb[x]);
                            contadorIncidencias++;
                            banderaPagina = true;
                            contadorIndices++;
                            found = "";

                        }
                      

                        else
                        {
                            found = "";
                        }


                    }
                    if (banderaPagina)
                    {
                       

                        listaPaginasResultado.Add(Program.arrayPaginasWeb[x]);
                        listaIncidenciasResultado.Add(Convert.ToString(contadorIncidencias));
                        listaTiempoResultado.Add(Convert.ToString(tiempoPagina.Elapsed));
                        listaPalabrasResultado.Add(Convert.ToString(busqueda));


                    }


                }


            }

            bandera = true;
            Console.WriteLine("Tiempo Secuencial: " + temporizador.Elapsed);
            
            return;
        }//fin busqueda secuencial



        public void busquedaParalela()
        {

            temporizador = Stopwatch.StartNew();
            string found = "";


            foreach (string busqueda in palabras)
            {
                Console.WriteLine("palabra a buscar es: " + busqueda);

                Parallel.ForEach(
                Program.arrayPaginasWeb,
                new ParallelOptions { MaxDegreeOfParallelism = cores },
                webpage =>
                {

                    bool banderaPagina = false;
                    Stopwatch tiempoPagina = Stopwatch.StartNew();
                    int contadorIncidencias = 0;

                    string pag = DescargarWeb(webpage);
                    Console.WriteLine("Buscando en...   "+ webpage);
                   
                    for (int i = 0; i < pag.Length; i++)
                    {
                        found = "";
                        int cont = i; //index auxiliar para concatenacion
                        int contadorWhile = 0; //contador de parada para el while
                        while (contadorWhile < busqueda.Length)
                        {
                            if (cont >= pag.Length)
                            {
                                break;
                            }
                            found += pag[cont];
                            cont++;
                            contadorWhile++;

                        }


                        if (found == busqueda)
                        {
                            Console.WriteLine("<<<<<<<<<<<<< "+busqueda+" <<<<<<<<<<<<<<<<<-->  " + found + " en pagina: " + webpage);
                          
                            found = "";
                            banderaPagina = true;
                            contadorIncidencias++;

                        }
                       


                        else                //si la palabra no es igual tiene que vaciar la variable
                        {
                            found = "";
                        }
                    }

                    if (banderaPagina)
                    {

                        listaPaginasResultado.Add(webpage);
                        listaIncidenciasResultado.Add(Convert.ToString(contadorIncidencias));
                        listaTiempoResultado.Add(Convert.ToString(tiempoPagina.Elapsed));
                        listaPalabrasResultado.Add(Convert.ToString(busqueda));

                    }





                }
            );
            }
            Console.WriteLine("Tiempo Paralelo: " + temporizador.Elapsed);
            
            this.label3.Text = "Tiempo Paralelo: " + temporizador.Elapsed;
        }//fin de busquedaParalela
    }
}
