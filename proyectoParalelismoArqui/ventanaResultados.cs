﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.Linq;
using System.Net;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace proyectoParalelismoArqui
{
    public partial class ventanaResultados : Form
    {

        int cores =0;
        //public static List<String> listaPaginasResultado = new List<string>();
        //public static List<String> listaIncidenciasResultado = new List<string>();
        //public static List<String> listaTiempoResultado = new List<string>();
        //public static List<String> listaPalabrasResultado = new List<string>();
        //public static string labelTiempo = "";

        static Stopwatch temporizador;
        public ventanaResultados()
        {
            InitializeComponent();

        }
        public ventanaResultados(int cantCores)
        {
            InitializeComponent();
            cores = cantCores;

        }
        private void listView1_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

       

        private void label2_Click(object sender, EventArgs e)
        {
            this.label2.Text = "sdf";



        }

        public static string DescargarWeb(string url)
        {

            var client = new WebClient { Encoding = System.Text.Encoding.UTF8 };            // { Encoding = System.Text.Encoding.UTF8 } es para que lea tildes y letra ñ
            var text = client.DownloadString(url);                                          //Desgarga la pagina web

            string webSinEtiquetasHTML = Regex.Replace(text, "<.*?>", String.Empty);        //para quitar los tags de html

            //Regex rRemScript = new Regex(@"<script[^>]*>[\s\S]*?</script>");
            //output = rRemScript.Replace(input, "");
            //Console.WriteLine(webSinEtiquetasHTML);
            //Console.WriteLine("**************************************************************************************************************");
            return webSinEtiquetasHTML;
        }//fin DescargarWeb


        public void buscarSecuencial()
        {

            
            string found = "";
            //string[] palabrasBusqueda = { "hoy" };
            int contadorIndices = 0;

            temporizador = Stopwatch.StartNew();


            foreach (string busqueda in main.palabras)
            {
                
                for (int x = 0; x < Program.arrayPaginasWeb.Length; x++)
                {
                  
                    int contadorIncidencias = 0;
                    bool banderaPagina = false;
                    Stopwatch tiempoPagina = Stopwatch.StartNew();


                    string pag = DescargarWeb(Program.arrayPaginasWeb[x]);

                    for (int i = 0; i < pag.Length; i++)
                    {

                        int cont = i; //index auxiliar para concatenacion
                        int contadorWhile = 0; //contador de parada para el while
                        while (contadorWhile < busqueda.Length) {
                            if (cont >= pag.Length) {
                                break;
                            }
                            found += pag[cont];
                            cont++;
                            contadorWhile++;

                        }

                        if (found == busqueda)
                        {
                            //found += palabra[i];
                            Console.WriteLine("<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<-->  " + found + " en pagina: " + Program.arrayPaginasWeb[x]);
                            //indicesPagEncontradas[contadorIndices] = x;
                            contadorIncidencias++;
                            banderaPagina = true;
                            //listBox1.Items.Add(Program.arrayPaginasWeb[x]);
                            contadorIndices++;
                            found = "";
                           
                        }
                        //else if (pag[i] != ';')
                        //{
                        //    found += pag[i];

                        //}


                        else
                        {
                            found = "";
                        }

                        
                    }
                    if (banderaPagina)
                    {
                        //listBox1.Items.Add(Program.arrayPaginasWeb[x] + "               incidencias: " + contadorIncidencias);
                        
                        ListViewItem list = new ListViewItem(Program.arrayPaginasWeb[x]);
                        list.SubItems.Add(Convert.ToString(contadorIncidencias));
                        list.SubItems.Add(Convert.ToString(tiempoPagina.Elapsed));
                        list.SubItems.Add(Convert.ToString(busqueda));
                        listView1.Items.Add(list);
                        

                    }


                }
                

            }
            //this.label2.Text = "Tiempo secuencial: " + temporizador.Elapsed;
            //this.Show();

            

        }//fin busqueda secuencial




        public void busquedaParalela()
        {
            temporizador = Stopwatch.StartNew();
            string found = "";

            Parallel.Invoke(() => {
                for (int cont = 0; cont < (Program.arrayPaginasWeb.Length / 2); cont++)
                {

                    bool banderaPagina = false;
                    Stopwatch tiempoPagina = Stopwatch.StartNew();
                    int contadorIncidencias = 0;

                    string pag = DescargarWeb(Program.arrayPaginasWeb[cont]);
                    //dValorPow1 += Math.Pow(cont, 2);

                    for (int i = 0; i < pag.Length; i++)
                    {
                        if (found == "hoy")
                        {
                            //found += palabra[i];
                            Console.WriteLine("<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<-->  " + found + " en pagina: " + Program.arrayPaginasWeb[cont]);
                            //indicesPagEncontradas[contadorIndices] = x;
                            //contadorIndices++;
                            //listBox1.Items.Add(Program.arrayPaginasWeb[cont]);
                            found = "";
                            banderaPagina = true;
                            contadorIncidencias++;
                            
                        }
                        else if (pag[i] != ' ')
                        {
                            found += pag[i];

                        }


                        else
                        {
                            //Console.WriteLine(found);
                            found = "";
                        }
                    }

                    if (banderaPagina)
                    {
                        //listBox1.Items.Add(Program.arrayPaginasWeb[x] + "               incidencias: " + contadorIncidencias);


                        ListViewItem list = new ListViewItem(Program.arrayPaginasWeb[cont]);
                        list.SubItems.Add(Convert.ToString(contadorIncidencias));
                        list.SubItems.Add(Convert.ToString(tiempoPagina.Elapsed));
                        list.SubItems.Add(Convert.ToString(found));
                        listView1.Items.Add(list);

                    }


                }
            }
            , () => {


                for (int cont = ((int)Program.arrayPaginasWeb.Length / 2); cont < Program.arrayPaginasWeb.Length; cont++)
                {

                    string pag = DescargarWeb(Program.arrayPaginasWeb[cont]);
                    //dValorPow2 += Math.Pow(cont, 2);
                    bool banderaPagina = false;
                    Stopwatch tiempoPagina = Stopwatch.StartNew();
                    int contadorIncidencias = 0;


                    for (int i = 0; i < pag.Length; i++)
                    {
                        if (found == "llamadas")
                        {
                            //found += palabra[i];
                            Console.WriteLine("<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<-->  " + found + " en pagina: " + Program.arrayPaginasWeb[cont]);
                            //indicesPagEncontradas[contadorIndices] = x;
                            //contadorIndices++;
                            //listBox1.Items.Add(Program.arrayPaginasWeb[cont]);
                            found = "";
                            banderaPagina = true;
                            contadorIncidencias++;
                            break;
                        }
                        else if (pag[i] != ' ')
                        {
                            found += pag[i];

                        }


                        else
                        {
                            //Console.WriteLine(found);
                            found = "";
                        }
                    }

                    if (banderaPagina)
                    {
                        //listBox1.Items.Add(Program.arrayPaginasWeb[x] + "               incidencias: " + contadorIncidencias);


                        ListViewItem list = new ListViewItem(Program.arrayPaginasWeb[cont]);
                        list.SubItems.Add(Convert.ToString(contadorIncidencias));
                        list.SubItems.Add(Convert.ToString(tiempoPagina.Elapsed));
                        list.SubItems.Add(Convert.ToString(found));
                        listView1.Items.Add(list);

                    }


                }
            });
            //this.label2.Text = "Tiempo de ejecución secuencial: " + temporizador.Elapsed + " milisegundos";
            //this.label3.Text = "Tiempo paralelo: " + temporizador.Elapsed;
            
            Console.Write("Tiempo paralelo: " + temporizador.Elapsed);
            //this.Show();
            //this.Visible = true;
        }//fin de busquedaParalela




        private void listView1_SelectedIndexChanged_1(object sender, EventArgs e)
        {

        }

        private void listView1_Click(object sender, EventArgs e)
        {
            Process.Start(listView1.SelectedItems[0].SubItems[0].Text);
            Console.WriteLine(listView1.SelectedItems[0].SubItems[0].Text);
        }
    }
}
